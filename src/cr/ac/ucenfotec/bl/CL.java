package cr.ac.ucenfotec.bl;

import java.util.ArrayList;

public class CL {

    private ArrayList<Carrera> listaCarreras;

    public CL(){
        listaCarreras = new ArrayList<>();
    }

    public String agregarCarrera(String codigo, String nombre, boolean esAcreditada){
        Carrera carreraNueva = new Carrera(codigo,nombre,esAcreditada);
        listaCarreras.add(carreraNueva);

        return "La carrera fue registrada exitosamente!";
    }
}
